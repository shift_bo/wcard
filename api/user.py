#coding=utf-8

from flask import Blueprint
from service.user import EmployeeService
from base import ApiJsonify as jsonify
from model import Department,db

bp = Blueprint('user',__name__)

class Helper(object):
    @classmethod
    def department_to_json(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "parentId": item.parent_id,
            "type": "DEPARTMENT",
        }

    @classmethod
    def employee_to_json(cls, item):
        return {
            "name": item.name,
            "usercode": item.usercode,
            "username":cls.formate_username(item.username),
            "avatar": item.avatar,
            "departmentId": item.department_id,
            "departmentName": cls.query_department_name(item.department_id),
            "roleId": item.role_id,
        }

    @classmethod
    def query_department_name(cls,item):
      data = Department.query.filter_by(uid = item).first()
      if data:
        return data.name
      return '无'
    @classmethod
    def formate_username(cls,username):
      username = username.split('.')
      username.reverse()
      print username,123123
      if len(username[0])==1:
        username.pop(0)
      return ".".join(username)

      
        
@bp.route('/currentuser')
def get_current_user():
  current_user = EmployeeService.current_user()
  currentUser = Helper.employee_to_json(current_user)
  return jsonify.ok(currentUser = currentUser)