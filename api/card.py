#coding=utf-8
from api.base import RestApi,RestView
import re
from flask import request,Response,current_app,make_response,send_from_directory,send_file
from model import Card,db,User
from sqlalchemy import or_,and_
import base64
import os
import json
from datetime import datetime
import time
from extends import corp_wx
import csv,codecs
import zipfile
from io import BytesIO
api = RestApi('w-card',__name__)

@api.route('/cardata')
class CardView(RestView):
  def post(self):
    """工卡信息提交"""
    newType = request.args.get('newType')
    username = request.json.get('username')
    usercode = request.json.get('usercode')
    name = request.json.get('name')
    userImage = request.json.get('userImage')
    originName = request.json.get('originName')
    department_name =request.json.get('departmentName')
    originType = request.json.get('originType')
    print originType
    img_base64 = userImage[0]['content']
    self.save_user_img(img_base64,usercode)
    if Card.query.filter(Card.usercode==usercode,Card.status==-1).first():
      return self.no(errmsg='请等待管理员通过工卡挂失请求')
    if Card.query.filter(Card.usercode==usercode,and_(Card.status!=4,Card.status!=-2)).first():
      return self.no(errmsg='请先挂失工卡再进行提交\n(请勿重复提交)')
    re_data = Card.query.filter(Card.usercode==usercode,or_(Card.status==4,Card.status==-2))
    if re_data.first():
      if newType == 'y':
        getUser = request.json.get('expressUser')
        phone = request.json.get('phone')
        place = request.json.get('place')
        re_data.update({
          "isMarket":True,
          "status" : 0,
          "getUser":getUser,
          "phone":phone,
          "place":place,
          "origin_type":originType,
          "commit_time":datetime.now()
        })
        db.session.commit()
        self.send_message(usercode)
        return self.ok()
      else:
        re_data.update({
          "isMarket":False,
          "origin_name":originName,
          "status" : 0,
          "commit_time":datetime.now(),
          "getUser":'',
          "phone":None,
          "place":'',
          "origin_type":originType
        })
        db.session.commit()
        self.send_message(usercode)
        return self.ok()
    if newType == 'n':
      card_data = Card(username=username,usercode=usercode,name=name,status=0,department_name=department_name,origin_name = originName,origin_type=originType)
      self.send_message(usercode)
      db.session.add(card_data)
      db.session.commit()
      return self.ok()
    else:
      getUser = request.json.get('expressUser')
      phone = request.json.get('phone')
      place = request.json.get('place')
      card_data = Card(username=username,usercode=usercode,name=name,status=0,isMarket=True,getUser=getUser,phone=phone,place=place,department_name=department_name,origin_type=originType)
      self.send_message(usercode)
      db.session.add(card_data)
      db.session.commit()
      return self.ok()
  def get(self):
    t = request.args.get('type')
    page = request.args.get('page')
    if t=='all':
      data = Card.query.order_by(Card.update_time.desc()).paginate(page=int(page),per_page=5)
      data = [self.card_data_to_json(item) for item in data.items]
      count = Card.query.count()
      return self.ok(card_list=data,count=count)
    elif t=='0':
      data = Card.query.filter_by(status=0).order_by(Card.update_time.desc()).paginate(page=int(page),per_page=5)
      data = [self.card_data_to_json(item) for item in data.items]
      count = Card.query.filter_by(status=0).count()
      return self.ok(card_list=data,count=count)
    elif t=='1':
      data = Card.query.filter_by(status=1).order_by(Card.update_time.desc()).paginate(page=int(page),per_page=5)
      data = [self.card_data_to_json(item) for item in data.items]
      count = Card.query.filter_by(status=1).count()
      return self.ok(card_list=data,count=count)
    elif t=='2':
      data = Card.query.filter_by(status=2).order_by(Card.update_time.desc()).paginate(page=int(page),per_page=5)
      data = [self.card_data_to_json(item) for item in data.items]
      count = Card.query.filter_by(status=2).count()
      return self.ok(card_list=data,count=count)
    elif t=='3':
      data = Card.query.filter_by(status=3).order_by(Card.update_time.desc()).paginate(page=int(page),per_page=5)
      data = [self.card_data_to_json(item) for item in data.items]
      count = Card.query.filter_by(status=3).count()
      return self.ok(card_list=data,count=count)
    elif t=='4':
      data = Card.query.filter_by(status=4).order_by(Card.update_time.desc()).paginate(page=int(page),per_page=5)
      data = [self.card_data_to_json(item) for item in data.items]
      count = Card.query.filter_by(status=4).count()
      return self.ok(card_list=data,count=count)
    elif t=='isRecard':
      data = Card.query.filter_by(isReCard=True).order_by(Card.update_time.desc()).paginate(page=int(page),per_page=5)
      data = [self.card_data_to_json(item) for item in data.items]
      count = Card.query.filter_by(isReCard=True).count()
      return self.ok(card_list=data,count=count)
    # elif t=='-2':
    #   data = Card.query.filter_by(status=-2).order_by(Card.update_time.desc()).paginate(page=int(page),per_page=4)
    #   data = [self.card_data_to_json(item) for item in data.items]
    #   if str(page)=='1':
    #     count = Card.query.filter_by(status=-2).count()
    #     return self.ok(card_list=data,count=count)
    #   return self.ok(card_list = data)
    # return self.ok()

  def send_message(self,usercode):
    # touser = User.query.filter_by(role_id=3).first().usercode
    message = {
       "touser" : current_app.config.get('ADMIN_USER'),
       "msgtype" : "textcard",
       "agentid" : 1000073,
       "textcard" : {
                "title" : "工卡申请通知",
                "description" : "<div class=\"gray\">"+time.strftime("%Y-%m-%d", time.localtime()) +"</div> <div class=\"normal\">您有一个新的工卡审批</div><div class=\"highlight\">请尽快处理</div>",
                "url" : "http://oa-notice.test.17zuoye.net/card/detail?usercode={usercode}".format(usercode=usercode),
                "btntxt":"详情"
       },
       "enable_id_trans": 0,
       "enable_duplicate_check": 0,
       "duplicate_check_interval": 1800
    }
    corp_wx.service.send_message(json.dumps(message))
  def save_user_img(self,content,usercode):
    content = content.split(',')[1]
    path = os.path.join(os.path.abspath('.'),'static')
    path = os.path.join(path,'userImg')
    path = path+'/'+str(usercode)+'.png'
    file = open(path,'wb')
    file.write(base64.b64decode(content))
    file.close
  def card_data_to_json(self,item):
    return {
      "username": item.username,
      "name":item.name,
      "usercode":item.usercode,
      "commitTime": str(item.commit_time),
      "rejectMsg": item.reject_msg,
      "status":item.status,
      "isMarket":item.isMarket,
      "getUser":item.getUser,
      "phone":item.phone,
      "place":item.place,
      "userType":self.userDisplay(item.user_type),
      "cardType":self.cardDisplay(item.card_type),
      "departmentName":item.department_name,
      "isRecard":item.isReCard,
      "originName":item.origin_name,
      "originType":item.origin_type
    }
  def userDisplay(self,userType):
    if userType == 'formal':
      return '正式员工'
    else:
      return '兼职'
  def cardDisplay(self,cardType):
    if cardType == 'formal':
      return '正式工卡'
    else:
      return '临时工卡'
@api.route('/cardstate')
class ChangeCardView(RestView):
  def post(self):
    status = request.json.get('state')
    card_data_list = request.json.get('cardDataList')
    for item in card_data_list:
      Card.query.filter_by(usercode = item["usercode"]).update({
        "status" : status
      })
      if status=='1':
        self.send_message("亲爱的17小伙伴，您已提交正式工卡申请,工卡正在快马加鞭的向您赶来，请您耐心等待呦~\n工卡制作周期:1周左右",item['usercode'])
      elif status=='2':
        self.send_message("您的工卡已制作完成，请到相应地点领取~\n\n绿地/中护航员工-绿地B座16层前台(于丙书）\n浦项员工-浦项A座7层前台（张倩）\n海淀客服-京仪科技大厦D座1层 （马聪）\n\n（区域办公室和教学服务部的员工工卡，我们将统一为您邮寄，请您耐心等待哦~）",item['usercode'])
      elif status=='4':
        reject_msg = request.json.get('rejectMsg')
        self.send_message("您的工卡申请已被驳回,原因:{reject_msg}".format(reject_msg=reject_msg),item['usercode'])  
      elif status=='-2':
        pass
      elif status=='-3':
        reject_msg = request.json.get('rejectMsg')
        self.send_message("您的工卡挂失申请已被驳回,原因:{reject_msg}".format(reject_msg=reject_msg),item['usercode']) 
    db.session.commit()
    return self.ok()
  def send_message(self,message,usercode):
    message =     {
       "touser" : usercode,
       "toparty" : "",
       "totag" : "",
       "msgtype" : "text",
       "agentid" : 1000073,
       "text" : {
           "content" : message
       },
       "safe":0,
       "enable_id_trans": 0,
       "enable_duplicate_check": 0,
       "duplicate_check_interval": 1800
    }
    corp_wx.service.send_message(json.dumps(message))

@api.route('/cardImg')
class CardImgView(RestView):
  def get(self):
    usercode = request.args.get('usercode')
    path = os.path.join(os.path.abspath('.'),'static')
    path = os.path.join(path,'userImg')
    path = path+'/'+str(usercode)+'.png'
    mime = 'image/png'
    try:
      with open(path, 'rb') as f:
        image = f.read()
        return Response(image, mimetype=mime)
    except:
      path = os.path.join(os.path.abspath('.'),'static')
      path = os.path.join(path,'userImg')
      path = path+'/'+'default'+'.png'
      with open(path, 'rb') as f:
        image = f.read()
        return Response(image, mimetype=mime)


@api.route('/cardDetial')
class CardDetialView(CardView):
  def get(self):
    usercode = request.args.get('usercode')
    card = Card.query.filter_by(usercode=usercode).first()
    if card:
      cardDetial = self.card_data_to_json(card)
      return self.ok(cardDetial = cardDetial)
    return self.no(errmsg='您还没有相关工卡申请')

@api.route('/sendmessagetouser')
class MessageView(RestView):
  def post(self):
    usercode_list = request.json.get('usercodeList')
    for item in usercode_list:
      message =     {
         "touser" : item,
         "toparty" : "",
         "totag" : "",
         "msgtype" : "text",
         "agentid" : 1000073,
         "text" : {
             "content" : "您的工卡已制作完成，请到相应地点领取~\n\n绿地员工-绿地B座16层前台(于丙书）\n浦项员工-浦项A座7层前台（张倩）\n海淀客服-京仪科技大厦D座1层 （马聪）\n\n（区域办公室和教学服务部的员工工卡，我们将统一为您邮寄，请您耐心等待哦~）"
         },
         "safe":0,
         "enable_id_trans": 0,
         "enable_duplicate_check": 0,
         "duplicate_check_interval": 1800
      }
    corp_wx.service.send_message(json.dumps(message))
    return self.ok()

@api.route('/recard')
class ReCardView(RestView):
  def post(self):
    username = request.json.get('username')
    usercode = request.json.get('usercode')
    name = request.json.get('name')
    cardType = request.json.get('cardType')
    userType = request.json.get('userType')
    origin_type = request.json.get('originType')
    departmentName = request.json.get('departmentName')
    if Card.query.filter_by(usercode=usercode,status=-2).first():
      return self.no(errmsg="请勿重复提交")
    if Card.query.filter_by(usercode=usercode).first():
      Card.query.filter_by(usercode=usercode).update({
      "card_type":cardType,
      "user_type":userType,
      "department_name":departmentName,
      "status":-2,
      "isReCard":True,
      "origin_type":origin_type
      })
      db.session.commit()
      # message = {
      #  "touser" : current_app.config.get('ADMIN_USER'),
      #  "msgtype" : "textcard",
      #  "agentid" : 1000073,
      #  "textcard" : {
      #           "title" : "工卡挂失通知",
      #           "description" : "<div class=\"gray\">"+time.strftime("%Y-%m-%d", time.localtime()) +"</div> <div class=\"normal\">您有一个工卡挂失申请</div><div class=\"highlight\">请尽快处理</div>",
      #           "url" : "http://oa-notice.test.17zuoye.net/card/detail?usercode={usercode}".format(usercode=usercode),
      #           "btntxt":"详情"
      #  },
      #  "enable_id_trans": 0,
      #  "enable_duplicate_check": 0,
      #  "duplicate_check_interval": 1800
      # }
      # corp_wx.service.send_message(json.dumps(message))
      message =  {
         "touser" : current_app.config.get('A_USER'),
         "toparty" : "",
         "totag" : "",
         "msgtype" : "text",
         "agentid" : 1000073,
         "text" : {
             "content" : "{name}的工卡正在挂失,员工编号:{usercode},请尽快关闭其工卡权限".format(name=name,usercode=usercode)
         },
         "safe":0,
         "enable_id_trans": 0,
         "enable_duplicate_check": 0,
         "duplicate_check_interval": 1800
      }
      corp_wx.service.send_message(json.dumps(message))
      return self.ok()
    data = Card(username=username,usercode=usercode,name=name,card_type=cardType,user_type=userType,department_name = departmentName,status=-2,isReCard=True,origin_type=origin_type)
    db.session.add(data)
    db.session.commit()
    # message = {
    #    "touser" : current_app.config.get('ADMIN_USER'),
    #    "msgtype" : "textcard",
    #    "agentid" : 1000073,
    #    "textcard" : {
    #             "title" : "工卡挂失通知",
    #             "description" : "<div class=\"gray\">"+time.strftime("%Y-%m-%d", time.localtime()) +"</div> <div class=\"normal\">您有一个工卡挂失申请</div><div class=\"highlight\">请尽快处理</div>",
    #             "url" : "http://oa-notice.test.17zuoye.net/card/detail?usercode={usercode}".format(usercode=usercode),
    #             "btntxt":"详情"
    #    },
    #    "enable_id_trans": 0,
    #    "enable_duplicate_check": 0,
    #    "duplicate_check_interval": 1800
    # }
    # corp_wx.service.send_message(json.dumps(message))
    message =  {
         "touser" :  current_app.config.get('A_USER'),
         "toparty" : "",
         "totag" : "",
         "msgtype" : "text",
         "agentid" : 1000073,
         "text" : {
             "content" : "{name}的工卡正在挂失,员工编号:{usercode},请尽快关闭其工卡权限".format(name=name,usercode=usercode)
         },
         "safe":0,
         "enable_id_trans": 0,
         "enable_duplicate_check": 0,
         "duplicate_check_interval": 1800
      }
    corp_wx.service.send_message(json.dumps(message))
    return self.ok()

  @api.route('/export')
  class ExportData(RestView):
    def get(self):
      path = os.path.join(os.path.abspath('.'),'static')
      path_t = os.path.join(path,'total.zip')
      return send_file(path_t,attachment_filename= 'total.zip',as_attachment = True)
    def post(self):
      usercodeList = request.json.get('usercodeList')
      path = os.path.join(os.path.abspath('.'),'static')
      user_path = os.path.join(path,'user')
      path_t = os.path.join(path,'total.zip')
      zip_file = zipfile.ZipFile(path_t,'w',zipfile.ZIP_DEFLATED)
      for item in usercodeList:
        data = Card.query.filter_by(usercode=item).first()
        self.data_to_excel(item,data,data.isMarket)
        zip_file.write(user_path+'/'+item+'.zip',data.usercode+'.zip')
      return self.ok()
      # response = make_response(send_from_directory(path, 'total.zip', as_attachment=True))
      # return send_file(path_t,attachment_filename= 'Name.zip',as_attachment = True)
      # return response
    def data_to_excel(self,usercode,data,isMarket):
      if isMarket:
        file_name = usercode+'地址.csv'
        path = os.path.join(os.path.abspath('.'),'static')
        path = os.path.join(path,'user_place')
        path = os.path.join(path,file_name)
        f=codecs.open(path,'wb','gbk')
        spamwriter = csv.writer(f)
        spamwriter.writerow(['收件人','地址','手机号'])
        spamwriter.writerow([data.getUser,data.place,data.phone])
        path = os.path.join(os.path.abspath('.'),'static')
        path = os.path.join(path,'user')
        path = os.path.join(path,usercode+'.zip')
        zip_file = zipfile.ZipFile(path,'w',zipfile.ZIP_DEFLATED)    
        path = os.path.join(os.path.abspath('.'),'static')
        imgPath = os.path.join(path,'userImg')
        user_place = os.path.join(path,'user_place')
        name = re.sub(r'\w','',data.name)
        username = re.sub('\.','',data.username)
        zip_file.write(imgPath+'/'+usercode+'.png',"0"+usercode+name+username+'.png')
        zip_file.write(user_place+'/'+usercode+'地址.csv',usercode+' .csv')
      else:
        path = os.path.join(os.path.abspath('.'),'static')
        path = os.path.join(path,'user')
        path = os.path.join(path,usercode+'.zip')
        zip_file = zipfile.ZipFile(path,'w',zipfile.ZIP_DEFLATED)    
        path = os.path.join(os.path.abspath('.'),'static')
        imgPath = os.path.join(path,'userImg')
        name = re.sub(r'\w','',data.name)
        username = re.sub('\.','',data.username)
        zip_file.write(imgPath+'/'+usercode+'.png',"0"+usercode+name+username+'.png')

      
        