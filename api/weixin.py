# coding=utf-8
from flask import current_app
from flask import make_response
from flask import redirect
from flask import request
from utils import encrypt_token
from base import RestApi
from base import RestView

from extends import corp_wx

api = RestApi("api_weixin", __name__)


@api.route("/weixin/sdk")
class WXSDKView(RestView):
    """企业微信SDK配置"""
    
    def get(self):
        url = request.args.get('u')
        config = corp_wx.service.get_jssdk_config(url)
        return self.ok(config=config)
@api.route("/weixin/oauth2/web")
class WXOauthWebLinkView(RestView):
    """ 微信网页授权链接"""

    def get(self):
        detail_card = request.args.get('card')
        if detail_card:
            redirect_uri = current_app.config.get("WX_OAUTH2_CALLBACK")+'?card='+str(detail_card)
        redirect_uri = current_app.config.get("WX_OAUTH2_CALLBACK")
        return redirect(corp_wx.service.make_web_oauth2_link(redirect_uri=redirect_uri))


@api.route("/weixin/oauth2/callback")
class WXOauthCallbackView(RestView):
    """ 微信网页授权回调地址"""

    def get(self):
        card = request.args.get('card')
        code = request.args.get("code")
        usercode = corp_wx.service.get_visitor_info(code)
        response = make_response()
        if not usercode:
            response.set_cookie("TOKEN", "", max_age=0)
        else:
            token = encrypt_token(usercode)
            response.set_cookie("TOKEN", token, max_age=86400 * 7)
        if card:
            response.headers["Location"] = "/card/detail"
        response.headers["Location"] = "/card/"
        return response, 302
