# coding=utf-8

from celery.schedules import crontab

DEBUG = True

#WX
SECRET_KEY = "OaNoticeSystem"
SITE = "http://oa-notice.test.17zuoye.net"
WX_CORP_ID = "ww496415a282ed8602"
WX_CORP_SECRET = "j4ZlBHfoZbjVdUp8DueQG8hD2-rvKqzGtXiLWu_VIbg"
WX_AGENT_ID = "1000073"
WX_OAUTH2_CALLBACK = SITE + "/wcard/v/weixin/oauth2/callback"
# REDIS
REDIS_URL = "redis://localhost:6379/0"

CELERY_BROKER_URL = "redis://localhost:6379/0"
CELERY_RESULT_BACKEND = "redis://localhost:6379/1"
CELERY_DEFAULT_QUEUE = 'wcard'
CELERY_DEFAULT_EXCHANGE = 'wcard'
CELERY_TIMEZONE = 'Asia/Shanghai'
CELERY_IMPORTS = ["tasks.employee"]
CELERYBEAT_SCHEDULE = {
    "sync":{
        "task":"tasks.employee.sync",
        "schedule":crontab(minute="*/60")
    }
}

# DB
DB_CONFIG = dict(host="127.0.0.1", db="wcard", username="root", password="81486125")
DB_URI = "mysql+pymysql://{}:{}@{}/{}?charset=utf8mb4".format(
    DB_CONFIG["username"], DB_CONFIG["password"], DB_CONFIG["host"], DB_CONFIG["db"]
)
SQLALCHEMY_DATABASE_URI = DB_URI
SQLALCHEMY_TRACK_MODIFICATIONS = True

# 测试用户
TEST_USERCODE = '6003'

#管理员
ADMIN_USER = '6003'
#关卡权限关闭管理员
A_USER = '4713'