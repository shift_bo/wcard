#coding=utf-8
from flask import request,current_app
from model import db,User
from utils import decrypt_token

class EmployeeService(object):
    @classmethod
    def current_user(cls):
        """当前用户"""
        token = request.cookies.get('TOKEN')
        test_usercode = current_app.config.get('TEST_USERCODE')
        if test_usercode:
            return cls.get_by_usercode(test_usercode)
        try:
            usercode = decrypt_token(token)['usercode']
        except:
            return
        return cls.get_by_usercode(usercode)
    
    @classmethod
    def get_by_usercode(cls, usercode):
        return User.query.filter_by(usercode=usercode).first()
