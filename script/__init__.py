#coding=utf-8

from model import User,Role
from extends import db
from extends import corp_wx
from app import app
from flask_migrate import Migrate
migrate = Migrate(app, db)
@app.cli.command()
def sync():
    """同步部门和员工"""
    from tasks.employee import sync
    sync()

@app.cli.command()
def init_role():
    """初始化权限"""
    role = [{'id':'1','name':'MEMBER','display_name':'用户'},{'id':'2','name':'MODELADMIN','display_name':'模块管理员'},
    {'id':'3','name':'SUPERADMIN','display_name':'超级管理员'},]
    for i in role:
        db.session.add(Role(name=i['name'],display_name=i['display_name']))
    else:
        db.session.commit()

# @app.cli.command()
# def init_origin():
#     """初始化区域"""
#     origin = [
#         {'id':'1','origin_name':'绿地B座'},
#         {'id':'2','origin_name':'浦项A座'},
#         {'id':'3','origin_name':'中护航'},
#         {'id':'4','origin_name':'海淀客服'},
#         ]
#     for i in origin:
#         db.session.add(Origin(origin_name=i['origin_name']))
#     else:
#         db.session.commit()