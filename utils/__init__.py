# coding=utf-8
import time

import jwt
from flask import current_app

def encrypt_token(usercode):
    """ 通过usercode获取token"""
    secret_key = current_app.config.get("SECRET_KEY")
    return jwt.encode(
        {"usercode": usercode, "timestamp": int(time.time())},
        secret_key,
        algorithm="HS256",
    )


def decrypt_token(token):
    secret_key = current_app.config.get("SECRET_KEY")
    return jwt.decode(token, secret_key, algorithms=["HS256"])

